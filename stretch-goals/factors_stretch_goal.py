def factors(number):
    # ==============
    # Your code here
    list = []
    for n in range(2, int(number/2) + 1):
        if number % n == 0:
            list.append(n)

    if len(list) == 0:
        return str(number) + ' is a prime number'

    return list
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
