def vowel_swapper(string):
    # ==============
    # Your code here
    count_a = 0
    count_e = 0
    count_i = 0
    count_o = 0
    count_u = 0
    original_string = list(string)

    for i in range(0, len(string)):
        if original_string[i] == 'a' or original_string[i] == 'A':
            count_a += 1
            if count_a == 2:
                original_string[i] = '4'
        elif original_string[i] == 'e' or original_string[i] == 'E':
            count_e += 1
            if count_e == 2:
                original_string[i] = '3'
        elif original_string[i] == 'i' or original_string[i] == 'I':
            count_i += 1
            if count_i == 2:
                original_string[i] = '!'
        elif original_string[i] == 'o':
            count_o += 1
            if count_o == 2:
                original_string[i] = 'ooo'
        elif original_string[i] == 'O':
            count_o += 1
            if count_o == 2:
                original_string[i] = '000'
        elif original_string[i] == 'u' or original_string[i] == 'U':
            count_u += 1
            if count_u == 2:
                original_string[i] = '|_|'

    modified_string = ''.join(original_string)

    return modified_string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
