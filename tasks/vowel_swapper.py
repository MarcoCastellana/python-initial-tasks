def vowel_swapper(string):
    # ==============
    # Your code here
    original_string = list(string)

    for i in range(0, len(string)):
        if original_string[i] == 'a' or original_string[i] == 'A':
            original_string[i] = '4'
        elif original_string[i] == 'e' or original_string[i] == 'E':
            original_string[i] = '3'
        elif original_string[i] == 'i' or original_string[i] == 'I':
            original_string[i] = '!'
        elif original_string[i] == 'o':
            original_string[i] = 'ooo'
        elif original_string[i] == 'O':
            original_string[i] = '000'
        elif original_string[i] == 'u' or original_string[i] == 'U':
            original_string[i] = '|_|'

    modified_string = ''.join(original_string)

    return modified_string
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console